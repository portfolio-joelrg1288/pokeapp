import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:zipdev/src/pages/welcome_screen.dart';
import 'package:zipdev/src/blocs/auth/auth_bloc.dart';
import 'package:zipdev/src/pages/login/login_page.dart';
import 'package:zipdev/src/pages/splash_screen_page.dart';
import 'package:zipdev/src/blocs/pokemon/pokemon_bloc.dart';
import 'package:zipdev/src/blocs/simple_bloc_observer.dart';
import 'package:zipdev/src/data/repositories/auth_repository.dart';
import 'package:zipdev/src/pages/pokemon-list/pokemon-list-page.dart';
import 'package:zipdev/src/data/repositories/pokemon_repository.dart';

void main() {
  Bloc.observer = SimpleBlocObserver();
  runApp(App());
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<AuthBloc>(create: (context) => AuthBloc(AuthRepository())),
        BlocProvider<PokemonBloc>(
          create: (context) => PokemonBloc(PokemonRepository()),
        ),
      ],
      child: MaterialApp(
        title: 'PokeApp',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          primaryColor: Color(0xFF1DCDB2),
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        initialRoute: 'splash',
        routes: {
          'login': (BuildContext context) => LoginPage(),
          'welcome': (BuildContext context) => WelcomeScreen(),
          'splash': (BuildContext context) => SplashScreenPage(),
          'pokemon-list': (BuildContext context) => PokemonListPage(),
        },
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}
