import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:zipdev/src/data/interfaces/ipokemon_repository.dart';
import 'package:zipdev/src/data/services/pokemon_service.dart';

import 'package:zipdev/src/models/pokemon_model.dart';

part 'pokemon_event.dart';

part 'pokemon_state.dart';

class PokemonBloc extends Bloc<PokemonEvent, PokemonState> {
  PokemonService _pokemonService;

  PokemonBloc(IPokemonRepository repository) : super(PokemonInitialState()) {
    _pokemonService = PokemonService(repository);
  }

  @override
  Stream<PokemonState> mapEventToState(
    PokemonEvent event,
  ) async* {
    yield PokemonLoadingState();
    switch (event.runtimeType) {
      case PokemonInitialStateEvent:
        yield PokemonInitialState();
        break;
      case RetrievePokemonEvent:
        yield* _mapRetrievePokemonToState(event);
        break;
    }
  }

  Stream<PokemonState> _mapRetrievePokemonToState(
      RetrievePokemonEvent event) async* {
    final pokemon = await _pokemonService.retrievePokemon(
      page: event.page,
      itemsPerPage: event.itemsPerPage,
    );
    yield RetrievePokemonState(pokemon, event.page);
  }
}
