part of 'pokemon_bloc.dart';

abstract class PokemonState {
  final int page;
  final List<PokemonModel> pokemon;

  PokemonState({this.pokemon = const [], this.page = 0});
}

@immutable
class PokemonInitialState extends PokemonState {
  PokemonInitialState() : super();

  List<Object> get props => [];
}

class PokemonLoadingState extends PokemonState {
  PokemonLoadingState() : super();

  List<Object> get props => [];
}

class RetrievePokemonState extends PokemonState {
  final int page;
  final List<PokemonModel> pokemon;

  RetrievePokemonState(this.pokemon, this.page)
      : super(pokemon: pokemon, page: page);

  List<Object> get props => [pokemon, page];
}
