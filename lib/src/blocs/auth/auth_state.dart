part of 'auth_bloc.dart';

abstract class AuthState {
  final UserModel user;
  final bool showWelcome;

  AuthState({this.user, this.showWelcome});
}

@immutable
class AuthInitialState extends AuthState {
  AuthInitialState({bool showWelcome = false})
      : super(showWelcome: showWelcome);

  List<Object> get props => [showWelcome];
}

class AuthVerifyUserSessionState extends AuthState {
  AuthVerifyUserSessionState() : super();

  List<Object> get props => [];
}

class AuthLoadingState extends AuthState {
  AuthLoadingState() : super();

  List<Object> get props => [];
}

class AuthSuccessState extends AuthState {
  final UserModel user;

  AuthSuccessState(this.user) : super(user: user);

  List<Object> get props => [user];
}
