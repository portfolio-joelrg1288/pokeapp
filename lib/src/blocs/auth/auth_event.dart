part of 'auth_bloc.dart';

abstract class AuthEvent {
  AuthEvent();
}

class AuthInitialStateEvent extends AuthEvent {
  AuthInitialStateEvent();

  List<Object> get props => [];
}

class AuthLoginEvent extends AuthEvent {
  final String email;
  final String password;

  AuthLoginEvent({@required this.email, @required this.password});

  List<Object> get props => [email, password];
}

class AuthVerifyUserSessionEvent extends AuthEvent {
  AuthVerifyUserSessionEvent();

  List<Object> get props => [];
}
