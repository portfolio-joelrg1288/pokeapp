import 'package:zipdev/src/models/pokemon_model.dart';
import 'package:zipdev/src/data/interfaces/ipokemon_repository.dart';

class PokemonService {
  final IPokemonRepository _repository;

  PokemonService(this._repository);

  Future<List<PokemonModel>> retrievePokemon({
    int page = 0,
    int itemsPerPage = 20,
  }) async {
    return await _repository.retrievePokemon(page, itemsPerPage);
  }
}
