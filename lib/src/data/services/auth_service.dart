import 'package:zipdev/src/models/user_model.dart';
import 'package:zipdev/src/data/interfaces/iauth_repository.dart';

class AuthService {
  final IAuthRepository _authRepository;

  AuthService(this._authRepository);

  Future<bool> userLoggedIn() async {
    return await _authRepository.userLoggedIn();
  }

  Future<bool> showWelcomeScreen() async {
    return await _authRepository.showWelcomeScreen();
  }

  Future<UserModel> login(String email, String password) async {
    try {
      return await _authRepository.login(email, password);
    } catch (ex) {
      print(ex);
      return null;
    }
  }
}
