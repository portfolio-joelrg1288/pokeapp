import 'dart:convert';

import 'package:zipdev/src/models/pokemon_model.dart';
import 'package:zipdev/src/data/interfaces/ipokemon_repository.dart';
import 'package:zipdev/src/data/repositories/generic_repository.dart';

class PokemonRepository extends IPokemonRepository {
  final _genericRepository = GenericRepository();

  @override
  Future<List<PokemonModel>> retrievePokemon(int page, int itemsPerPage) async {
    final uri = Uri.https('pokeapi.co', '/api/v2/pokemon',
        {'limit': '$itemsPerPage', 'offset': '${page * itemsPerPage}'});
    final res = await _genericRepository.get(uri.toString());
    final body = json.decode(res.body);
    final List<PokemonModel> result = [];
    for (var e in body['results']) {
      final PokemonModel pokemon = await retrieveOnePokemon(e['url']);
      result.add(pokemon);
    }
    return result;
  }

  @override
  Future<PokemonModel> retrieveOnePokemon(String path) async {
    final res = await _genericRepository.get(path);
    final body = json.decode(res.body);
    return PokemonModel.fromJson(body);
  }
}
