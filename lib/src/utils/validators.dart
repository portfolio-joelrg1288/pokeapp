abstract class Validators {
  static String emailValidator(String email) {
    email = email.trim();
    Pattern pattern =
        r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
    RegExp regExp = RegExp(pattern);
    if (email.length == 0)
      return "Email is required";
    else if (!regExp.hasMatch(email)) return "Invalid Email";
    return null;
  }

  static String passwordValidator(String password) {
    if (password.isEmpty) return "Password is required";
    if (password.length < 6) return "Password is too short";
    return null;
  }
}
