class UserModel {
  String id;
  String email;

  UserModel({this.id, this.email});

  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'email': email,
    };
  }
}
