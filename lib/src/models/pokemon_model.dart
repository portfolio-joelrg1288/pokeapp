class PokemonModel {
  int id;
  int height;
  int weight;
  String name;
  List<String> moves;
  List<String> types;
  List<String> sprites;
  List<String> abilities;

  PokemonModel({
    this.id = 0,
    this.name = '',
    this.height = 0,
    this.weight = 0,
    this.moves = const [],
    this.types = const [],
    this.sprites = const [],
    this.abilities = const [],
  });

  static PokemonModel fromJson(Map<String, dynamic> map) {
    if (map == null) return null;
    final List<String> movesList = [];
    final List<String> typesList = [];
    final List<String> abilitiesList = [];
    map['types'].forEach((e) => typesList.add(e['type']['name']));
    map['moves'].forEach((e) => movesList.add(e['move']['name']));
    map['abilities'].forEach((e) => abilitiesList.add(e['ability']['name']));
    return PokemonModel(
      id: map['id'],
      name: map['name'],
      height: map['height'],
      weight: map['weight'],
      sprites: [
        map['sprites']['front_default'],
        map['sprites']['back_default'],
      ],
      moves: movesList,
      types: typesList,
      abilities: abilitiesList,
    );
  }
}
