import 'package:flutter/material.dart';
import 'package:transparent_image/transparent_image.dart';

import 'package:zipdev/src/models/pokemon_model.dart';
import 'package:zipdev/src/pages/pokemon-details/pokemon-details-screen.dart';

class PokemonCard extends StatelessWidget {
  final PokemonModel pokemonModel;

  PokemonCard({this.pokemonModel});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => PokemonDetailsScreen(pokemon: pokemonModel)),
      ),
      child: FractionallySizedBox(
        widthFactor: 0.46,
        child: Container(
          padding: EdgeInsets.only(bottom: 10, left: 10, right: 10),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(5),
              boxShadow: [
                BoxShadow(
                  blurRadius: 5,
                  spreadRadius: 0,
                  offset: Offset(3, 5),
                  color: Color(0xFF149A85).withOpacity(0.2),
                )
              ]),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FadeInImage(
                placeholder: MemoryImage(kTransparentImage),
                image: NetworkImage(pokemonModel.sprites[0]),
                height: MediaQuery.of(context).size.height * 0.15,
              ),
              Container(
                  width: double.infinity,
                  child: Text(
                    pokemonModel.name,
                    style: TextStyle(
                      fontSize: 16,
                      letterSpacing: 2,
                      fontFamily: 'Pokemon',
                    ),
                  )),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text(
                    'Height:',
                    textAlign: TextAlign.end,
                    style: TextStyle(fontSize: 12),
                  ),
                  Text(
                    '${pokemonModel.height / 10}m',
                    textAlign: TextAlign.end,
                    style: TextStyle(
                      fontSize: 12,
                      color: Color(0xFF149A85),
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 5),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text(
                    'Weight:',
                    textAlign: TextAlign.end,
                    style: TextStyle(fontSize: 12),
                  ),
                  Text(
                    '${pokemonModel.weight / 10}kg',
                    textAlign: TextAlign.end,
                    style: TextStyle(
                      fontSize: 12,
                      color: Color(0xFF149A85),
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
