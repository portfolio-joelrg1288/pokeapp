import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:zipdev/src/blocs/auth/auth_bloc.dart';

class SplashScreenPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthBloc, AuthState>(
      listener: (_, AuthState state) {
        if (state is AuthInitialState) {
          Navigator.pushReplacementNamed(
              context, state.showWelcome ? 'welcome' : 'login');
        }
        if (state is AuthSuccessState) {
          Navigator.pushReplacementNamed(context, 'pokemon-list');
        }
      },
      builder: (context, state) {
        if (state is AuthVerifyUserSessionState) {
          BlocProvider.of<AuthBloc>(context).add(AuthVerifyUserSessionEvent());
        }
        return Scaffold(
          body: Container(
            decoration: BoxDecoration(
                gradient: LinearGradient(colors: [
              Color(0xFFA2F6B7),
              Color(0xFF1CF2D1),
            ])),
            constraints: BoxConstraints.expand(),
            child: SafeArea(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Flexible(
                    child: FractionallySizedBox(
                        widthFactor: 0.7,
                        child:
                            Image(image: AssetImage('assets/images/logo.png'))),
                  ),
                  CircularProgressIndicator(
                    valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
                  ),
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
