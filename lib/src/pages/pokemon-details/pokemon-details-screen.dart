import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

import 'package:zipdev/src/models/pokemon_model.dart';
import 'package:zipdev/src/pages/pokemon-details/widgets/details_expansion_tile.dart';

class PokemonDetailsScreen extends StatefulWidget {
  final PokemonModel pokemon;

  PokemonDetailsScreen({@required this.pokemon});

  @override
  _PokemonDetailsScreenState createState() => _PokemonDetailsScreenState();
}

class _PokemonDetailsScreenState extends State<PokemonDetailsScreen> {
  int selectedPage = 0;
  final CarouselController carouselController = CarouselController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: double.infinity,
        height: double.infinity,
        child: SingleChildScrollView(
          child: SafeArea(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () => Navigator.pop(context),
                      child: Container(
                        padding: EdgeInsets.all(8),
                        margin: EdgeInsets.only(left: 20),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          gradient: LinearGradient(
                            colors: [
                              Color(0xFFA2F6B7),
                              Color(0xFF1CF2D1),
                            ],
                          ),
                        ),
                        child: Icon(
                          Icons.arrow_back_ios,
                          color: Colors.white,
                        ),
                      ),
                    ),
                  ],
                ),
                CarouselSlider(
                  carouselController: carouselController,
                  items: widget.pokemon.sprites
                      .map((e) => Image(image: NetworkImage(e)))
                      .toList(),
                  options: CarouselOptions(
                    enableInfiniteScroll: false,
                    carouselController: carouselController,
                    onPageChanged: (value, reason) =>
                        this.setState(() => selectedPage = value),
                    height: MediaQuery.of(context).size.height * 0.15,
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: widget.pokemon.sprites
                      .map((e) => GestureDetector(
                            onTap: () => carouselController
                                .jumpToPage(widget.pokemon.sprites.indexOf(e)),
                            child: Image(
                              image: NetworkImage(e),
                              color: selectedPage ==
                                      widget.pokemon.sprites.indexOf(e)
                                  ? null
                                  : Colors.blue,
                              width: 40,
                            ),
                          ))
                      .toList(),
                ),
                SizedBox(height: 30),
                Text(
                  widget.pokemon.name.toUpperCase(),
                  style: TextStyle(
                    fontSize: 40,
                    letterSpacing: 3,
                    fontFamily: 'Pokemon',
                    color: Theme.of(context).primaryColor,
                  ),
                ),
                SizedBox(height: 20),
                DetailsExpansionTile(
                  title: 'Abilities',
                  details: widget.pokemon.abilities,
                ),
                DetailsExpansionTile(
                  title: 'Types',
                  details: widget.pokemon.types,
                ),
                DetailsExpansionTile(
                  title: 'Moves',
                  details: widget.pokemon.moves,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
