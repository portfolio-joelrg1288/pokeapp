import 'package:flutter/material.dart';

class DetailsExpansionTile extends StatelessWidget {
  final String title;
  final List<String> details;

  DetailsExpansionTile({this.title = '', this.details = const []});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      padding: EdgeInsets.only(left: 15),
      child: Theme(
        data: ThemeData(
          splashColor: Colors.transparent,
          dividerColor: Colors.transparent,
          highlightColor: Colors.transparent,
        ),
        child: ExpansionTile(
          title: Text(
            title,
            style: TextStyle(
              fontSize: 20,
              fontWeight: FontWeight.w600,
            ),
          ),
          children: details
              .map((e) => Column(
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(bottom: 5, top: 30),
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(
                              color: Theme.of(context).primaryColor,
                            ),
                          ),
                        ),
                        child: Text(
                          e.replaceAll('-', ' ').toUpperCase(),
                          style: TextStyle(fontSize: 15),
                        ),
                      ),
                    ],
                  ))
              .toList(),
        ),
      ),
    );
  }
}
