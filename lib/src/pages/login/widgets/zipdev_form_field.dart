import 'package:flutter/material.dart';

class ZipdevFormField extends StatelessWidget {
  final String label;
  final bool obscure;
  final TextInputType textInputType;
  final TextEditingController controller;
  final String Function(String) validator;

  ZipdevFormField({
    @required this.validator,
    @required this.controller,
    this.label,
    this.obscure = false,
    this.textInputType = TextInputType.emailAddress,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      obscureText: obscure,
      validator: validator,
      controller: controller,
      keyboardType: textInputType,
      decoration: InputDecoration(
        labelText: label,
        contentPadding: EdgeInsets.all(0),
      ),
    );
  }
}
